# frozen_string_literal: true

require_relative '../triage/triage'
require_relative './constants/labels'

module PipelineIncidentHelper
  class << self
    ROOT_CAUSE_LABEL_PREFIX = 'master-broken::'

    def closed_as_duplicate_of(resource)
      resource.dig('_links', 'closed_as_duplicate_of')
    end

    def apply_root_cause_label_from_duplicate_incident(resource)
      duplicate_incident_url = closed_as_duplicate_of(resource)

      root_cause_label = incident_payload_from_url(duplicate_incident_url).labels.find do |label|
        label.start_with?(ROOT_CAUSE_LABEL_PREFIX)
      end

      return unless root_cause_label && root_cause_label != Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS[:default]

      <<~MSG.chomp
       /label ~"#{root_cause_label}"
      MSG
    end

    private

    def incident_payload_from_url(url)
      matches = url.match(%r{/projects/(?<project_id>\d+)/issues/(?<incident_iid>\d+)})
      return unless matches

      Triage.api_client.issue(matches[:project_id], matches[:incident_iid])
    end
  end
end
