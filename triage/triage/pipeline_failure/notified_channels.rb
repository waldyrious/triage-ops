# frozen_string_literal: true

require_relative '../../../lib/hierarchy/group'
require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class NotifiedChannels
      FRONTEND_CHANNEL = 'frontend'
      QA_MASTER_CHANNEL = 'qa-master'

      attr_reader :channels

      def initialize(config, label_name)
        @label_name = label_name
        @config = config
      end

      def to_a
        channels = config.default_slack_channels.dup

        channels.push(group_channel) if config.post_to_group_channel? && config.auto_triage? && group_channel

        channels
      end

      def to_s
        to_a.map { |channel| "`##{channel}`" }.join(', ')
      end

      private

      attr_reader :label_name, :config

      def group
        Hierarchy::Group.find_by_label(label_name)
      end

      def group_channel
        return FRONTEND_CHANNEL if frontend? && group.nil?
        return QA_MASTER_CHANNEL if e2e? && group.nil?

        group&.slack_channel
      end

      def frontend?
        label_name == Labels::FRONTEND_LABEL
      end

      def e2e?
        label_name == Labels::RSPEC_TEST_LEVEL_LABELS[:e2e]
      end
    end
  end
end
