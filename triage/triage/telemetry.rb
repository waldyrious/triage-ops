# frozen_string_literal: true

require 'benchmark'
require 'gitlab-sdk'
require 'logger'
require 'snowplow-tracker'

module Triage
  class Telemetry
    ANALYTICS_BASE_URL = 'https://collector.prod-1.gl-product-analytics.com'

    def self.record_processor_execution(event:, processor:)
      return yield unless telemetry_enabled?

      result = nil

      duration = Benchmark.realtime { result = yield }.round(3)
      client.identify(event.event_actor_username || '?')
      client.track(processor.class.to_s, { duration: })

      result
    end

    def self.client
      return @client if @client

      app_id = ENV.fetch('GITLAB_SDK_APP_ID')
      host = ENV.fetch('GITLAB_SDK_HOST', ANALYTICS_BASE_URL)

      SnowplowTracker::LOGGER.level = Logger::WARN
      @client = GitlabSDK::Client.new(app_id:, host:)
    end
    private_class_method :client

    def self.telemetry_enabled?
      ENV['GITLAB_SDK_APP_ID'].present?
    end
    private_class_method :telemetry_enabled?
  end
end
