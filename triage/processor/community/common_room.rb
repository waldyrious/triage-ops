# frozen_string_literal: true

require_relative 'community_processor'
require_relative '../../job/common_room_job'

module Triage
  class CommonRoom < CommunityProcessor
    react_to 'merge_request.note', 'issue.note'

    ONE_MINUTE = 60

    def applicable?
      event.wider_community_author? &&
        !event.confidential? &&
        (event.from_gitlab_org? || event.from_gitlab_com?)
    end

    def process
      CommonRoomJob.perform_async(event)
    end

    def documentation
      <<~TEXT
        Queue community notes up to be sent to Common Room.
      TEXT
    end
  end
end
