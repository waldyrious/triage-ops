# frozen_string_literal: true

require_relative './base_job'

module Triage
  module PipelineIncidentSla
    class StageWarningJob < BaseJob
      DELAY = 1.5 * 60 * 60 # 1.5 hours
      MINUTES_BEFORE_ESCALATION = 30

      SLACK_PAYLOAD_TEMPLATE = <<~MESSAGE.chomp
        I'm requesting help on behalf of `#%<group_channel>s` to triage pipeline incident <%<incident_url>s|#%<incident_iid>s>.
        The incident has not received any update and will be escalated to <#%<dev_escalation_channel_id>s> in %<minutes_before_escalation>s minutes of inactivity (except weekends and holidays).
        Thanks for your attention!
      MESSAGE

      private

      def applicable?
        super && attributed_group
      end

      def threshold_in_seconds
        DELAY
      end

      def slack_channel
        stage_slack_channel
      end

      def slack_message
        format(SLACK_PAYLOAD_TEMPLATE,
          group_channel: group_slack_channel,
          incident_iid: event.iid,
          incident_url: event.url,
          dev_escalation_channel_id: DEV_ESCALATION_CHANNEL_ID,
          minutes_before_escalation: MINUTES_BEFORE_ESCALATION
        )
      end
    end
  end
end
