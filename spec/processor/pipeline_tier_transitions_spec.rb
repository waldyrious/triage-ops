# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/pipeline_tier_transitions'
require_relative '../../triage/triage/event'

RSpec.describe Triage::PipelineTierTransitions do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid) { 300 }

    let(:approved) { false }
    let(:approvals_required) { 2 }
    let(:approvals_left) { 2 }
    let(:approvers) { [] }

    let(:approvals) do
      Gitlab::ObjectifiedHash.new(
        approved: approved,
        approvals_required: approvals_required,
        approvals_left: approvals_left
      )
    end

    let(:label_names)             { ['bug::availability'] }
    let(:team_member_author)      { true }
    let(:known_automation_author) { false }
    let(:maybe_automation_author) { false }

    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approved',
        gitlab_bot_event_actor?: false,
        from_gitlab_org_gitlab?: true,
        team_member_author?: team_member_author,
        known_automation_author?: known_automation_author,
        maybe_automation_author?: maybe_automation_author,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: false,
        approvals: approvals,
        approvers: approvers,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  let(:instance) { described_class.new(event) }

  let(:labels_from_api) { [] }
  let(:merge_request_from_api) do
    { 'labels' => labels_from_api }
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  subject { instance }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}",
      response_body: merge_request_from_api)

    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'registers listeners', [
    'merge_request.open',
    'merge_request.approval',
    'merge_request.approved',
    'merge_request.unapproval',
    'merge_request.unapproved'
  ]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event project is under gitlab-org/security/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
        allow(event).to receive(:from_gitlab_org_security_gitlab?)
          .and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when the event is coming from a bot' do
      before do
        allow(event).to receive(:gitlab_bot_event_actor?)
          .and_return(true)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when the MR does not need a tier label' do
      before do
        fake_instance = instance_double(Triage::NeedMrApprovedLabel)
        allow(Triage::NeedMrApprovedLabel).to receive(:new).and_return(fake_instance)
        allow(fake_instance).to receive(:need_mr_approved_label?).and_return(false)
      end

      context 'when a tier label is not present on the MR' do
        let(:label_names) { [] }

        it 'does not do anything' do
          expect_no_request { subject.process }
        end
      end

      context 'when a tier label is present on the MR' do
        let(:label_names) { [Labels::PIPELINE_TIER_1_LABEL] }

        it 'removes the tier label' do
          body = <<~MARKDOWN.chomp
            /unlabel ~\"#{Labels::PIPELINE_TIER_1_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end

    context 'when the MR has a higher tier label than the next tier label' do
      let(:label_names) do
        [Labels::PIPELINE_TIER_3_LABEL, Labels::MR_APPROVED_LABEL]
      end

      it 'does not do anything' do
        expect_no_request { subject.process }
      end
    end

    describe 'adding ~pipeline::tier-X labels' do
      let(:approved)           { false }
      let(:approvals_required) { 2 }
      let(:approvals_left)     { 2 }
      let(:approvers)          { [] }

      before do
        stub_api_request(
          path: "/projects/12345/issues/6789/notes",
          query: { per_page: 100 },
          response_body: []
        )
      end

      context 'when the MR does not require any approvals' do
        let(:approvals_required) { 0 }
        let(:approvals_left)     { 0 }

        context 'when the MR is not yet approved' do
          let(:approvers) { [] }

          it 'adds the pipeline::tier-3 label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_3_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        context 'when the MR is first approved' do
          let(:approvers) { [{ 'id' => 567, 'username' => 'julie' }] }

          it 'adds the pipeline::tier-3 label and the Labels::MR_APPROVED_LABEL label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_3_LABEL}\" ~\"#{Labels::MR_APPROVED_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end
      end

      context 'when the MR was not approved by anybody' do
        let(:approvers) { [] }

        it 'adds the pipeline::tier-1 label' do
          body = <<~MARKDOWN.chomp
            /label ~\"#{Labels::PIPELINE_TIER_1_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end

        context 'when the Labels::MR_APPROVED_LABEL label is not present' do
          let(:label_names) { [] }

          it 'does not add the Labels::MR_APPROVED_LABEL label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_1_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        context 'when the tier label is already present' do
          let(:label_names) { [Labels::PIPELINE_TIER_1_LABEL] }

          it 'does not add the same tier label again' do
            expect(subject).not_to receive(:add_comment)

            subject.process
          end
        end
      end

      context 'when the MR was approved, but still needs some approvals' do
        let(:approved)       { false } # This field is only true when all the required approvals were given
        let(:approvals_left) { 1 }
        let(:approvers)      { [{ 'id' => 567, 'username' => 'julie' }] }
        let(:label_names)    { [Labels::MR_APPROVED_LABEL] } # we assume this label was already present by default

        it 'adds the pipeline::tier-2 label' do
          body = <<~MARKDOWN.chomp
            /label ~\"#{Labels::PIPELINE_TIER_2_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end

        context 'when the Labels::MR_APPROVED_LABEL label is not present' do
          let(:label_names) { [] }

          it 'also adds the Labels::MR_APPROVED_LABEL label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_2_LABEL}\" ~\"#{Labels::MR_APPROVED_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        context 'when the tier label is already present' do
          before do
            label_names << Labels::PIPELINE_TIER_2_LABEL
          end

          it 'does not add the same tier label again' do
            expect(subject).not_to receive(:add_comment)

            subject.process
          end
        end
      end

      context 'when the MR has all the approvals it needs' do
        let(:approved)       { true }
        let(:approvals_left) { 0 }
        let(:approvers) do
          [
            { 'id' => 567, 'username' => 'julie' },
            { 'id' => 123, 'username' => 'bob' }
          ]
        end
        let(:label_names) { [Labels::MR_APPROVED_LABEL] } # we assume this label was already present by default

        it 'adds the pipeline::tier-3 label' do
          body = <<~MARKDOWN.chomp
            /label ~\"#{Labels::PIPELINE_TIER_3_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end

        context 'when the Labels::MR_APPROVED_LABEL label is not present' do
          let(:label_names) { [] }

          it 'also adds the Labels::MR_APPROVED_LABEL label' do
            body = <<~MARKDOWN.chomp
              /label ~\"#{Labels::PIPELINE_TIER_3_LABEL}\" ~\"#{Labels::MR_APPROVED_LABEL}\"
            MARKDOWN

            expect_comment_request(event: event, body: body) do
              subject.process
            end
          end
        end

        context 'when the tier label is already present' do
          before do
            label_names << Labels::PIPELINE_TIER_3_LABEL
          end

          it 'does not add the same tier label again' do
            expect(subject).not_to receive(:add_comment)

            subject.process
          end
        end
      end

      context 'when the approvals are not as we expect them to be' do
        # Made-up, probably invalid configuration, to test a scenario where
        # the approvals are not as we expect them to be.
        let(:approved)       { false }
        let(:approvals_left) { 0 }
        let(:approvers) do
          [{ 'id' => 567, 'username' => 'julie' }]
        end
        let(:label_names) { [Labels::MR_APPROVED_LABEL] } # we assume this label was already present by default

        it 'adds the pipeline::tier-1 label' do
          body = <<~MARKDOWN.chomp
            /label ~\"#{Labels::PIPELINE_TIER_1_LABEL}\"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end

    describe 'triggering a new pipeline' do
      # Going from tier 1 to tier 2
      let(:approvals_left) { 1 }
      let(:approvers)      { [{ 'id' => 567, 'username' => 'julie' }] }
      let(:label_names)    { [Labels::PIPELINE_TIER_1_LABEL] }

      # There is a previous comment by default
      let(:no_previous_comment?) { false }

      # We isolate the pipeline trigger for this block.
      before do
        allow(subject).to receive(:add_comment)
        allow(instance).to receive_message_chain(:unique_comment, :no_previous_comment?).and_return(no_previous_comment?)
      end

      context 'when the author is a team member' do
        let(:team_member_author)      { true }
        let(:known_automation_author) { false }

        it 'triggers a new pipeline' do
          expect(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_in)

          subject.process
        end
      end

      context 'when the author is a known automation' do
        let(:team_member_author)      { false }
        let(:known_automation_author) { true }

        it 'triggers a new pipeline' do
          expect(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_in)

          subject.process
        end
      end

      context 'when the author is not a known automation' do
        let(:team_member_author)      { false }
        let(:known_automation_author) { false }

        it 'does not trigger a new pipeline' do
          expect(Triage::TriggerPipelineOnApprovalJob).not_to receive(:perform_in)

          subject.process
        end
      end
    end

    describe 'creating the pipeline tier comment' do
      # Going from tier 1 to tier 2
      let(:approvals_left)      { 1 }
      let(:approvers)           { [{ 'id' => 567, 'username' => 'julie' }] }
      let(:label_names)         { [Labels::PIPELINE_TIER_1_LABEL] }

      # There is a previous comment by default
      let(:no_previous_comment?) { false }

      # We isolate the pipeline tier comment creation for this block.
      before do
        allow(subject).to receive(:add_comment).with(
          '/label ~"pipeline::tier-2" ~"pipeline:mr-approved"',
          append_source_link: false
        )

        allow(subject).to receive(:add_comment).with('our stubbed message', append_source_link: false).and_call_original

        allow(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_in)
        allow(instance).to receive_message_chain(:unique_comment, :no_previous_comment?).and_return(no_previous_comment?)
      end

      context 'when the previous comment is absent' do
        let(:no_previous_comment?) { true }

        context 'when a new pipeline will be triggered automatically' do
          let(:team_member_author)      { true }
          let(:known_automation_author) { false }

          let(:body) do
            <<~MARKDOWN.strip
              ## Before you set this MR to auto-merge

              This merge request will progress on pipeline tiers until it reaches the last tier: ~"pipeline::tier-3".
              We will trigger a new pipeline for each transition to a higher tier.

              **Before you set this MR to auto-merge**, please check the following:

              * You are **the last maintainer** of this merge request
              * The latest pipeline for this merge request is ~"pipeline::tier-3" (You can find which tier it is in the pipeline name)
              * This pipeline is recent enough (**created in the last 8 hours**)

              If all the criteria above apply, please set auto-merge for this merge request.

              See [pipeline tiers](https://docs.gitlab.com/ee/development/pipelines/#pipeline-tiers) and [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) for more details.
            MARKDOWN
          end

          it 'creates the pipeline tier comment' do
            allow(instance).to receive_message_chain(:unique_comment, :wrap).with(body).and_return('our stubbed message')

            expect_comment_request(event: event, body: 'our stubbed message') do
              subject.process
            end
          end
        end

        context 'when a new pipeline will not be triggered automatically' do
          let(:team_member_author)      { false }
          let(:known_automation_author) { false }

          let(:body) do
            <<~MARKDOWN.strip
              ## Before you set this MR to auto-merge

              This merge request will progress on pipeline tiers until it reaches the last tier: ~"pipeline::tier-3".


              **Before you set this MR to auto-merge**, please check the following:

              * You are **the last maintainer** of this merge request
              * The latest pipeline for this merge request is ~"pipeline::tier-3" (You can find which tier it is in the pipeline name)
              * This pipeline is recent enough (**created in the last 8 hours**)

              If all the criteria above apply, please set auto-merge for this merge request.

              See [pipeline tiers](https://docs.gitlab.com/ee/development/pipelines/#pipeline-tiers) and [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) for more details.
            MARKDOWN
          end

          it 'creates the pipeline tier comment' do
            allow(instance).to receive_message_chain(:unique_comment, :wrap).with(body).and_return('our stubbed message')

            expect_comment_request(event: event, body: 'our stubbed message') do
              subject.process
            end
          end
        end
      end

      context 'when the previous comment is already present' do
        it 'does not create the pipeline tier comment' do
          expect_no_request { subject.process }
        end
      end
    end
  end
end
